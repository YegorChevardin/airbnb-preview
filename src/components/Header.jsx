import banner from "/src/assets/resources/images/banner.png"

export default function Header() {
    return (
        <div className="container">
            <div className="row justify-content-center align-items-center">
                <div className="col-md-6">
                    <div className="mt-5">
                        <img className="img-fluid" src={banner} alt="posts"/>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-7">
                    <div id="header-content" className="mt-5 mb-5">
                        <h1>Online Experiences</h1>
                        <p>
                            Join unique interactive activities led by <br/>
                            one-of-a-kind hosts—all without leaving<br/> home.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
}