import star from "../assets/resources/images/star.svg";

export default function Post(props) {
    let soldOutElement;
    if (props.soldOut) {
        soldOutElement = (
            <p className="text-danger m-0 mt-2"><small>sold out</small></p>
        )
    }
    return (
        <div className="col-sm-6 col-lg-2 col-md-3 mb-4">
            <div className="card border-radius border-0 shadow-sm ">
                <img className="bd-placeholder-img card-img-top border-img-radius overflow-hidden" src={props.postImage} alt="post"/>
                <div className="card-body mt-2">
                    <div className="d-flex align-items-center justify-content-start">
                        <img className="me-1" width="20" src={star} alt="star"/>
                        <p className="h5 m-0">{props.stars} <span className="text-muted">({Math.ceil(props.stars)}) • {props.country}</span></p>
                    </div>
                    <h5 className="card-title mt-3">{name}</h5>
                    <p className="h5 card-title mt-3"><strong>From ${props.cost}</strong> / person</p>
                    {soldOutElement}
                </div>
            </div>
        </div>
    )
}