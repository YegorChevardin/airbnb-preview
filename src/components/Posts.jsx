import Post from "./Post"
import postImage from "/src/assets/resources/images/post-example.png";

export default function Posts() {
    let posts = [];
    for (let i = 10; i > 0; i--) {
        posts.push(
            {
                key: i,
                name: "Life Lessons with Katie Zaferes",
                postImage: postImage,
                stars:5,
                country:"USA",
                cost:136,
                soldOut:true
            }
        );
    }

    const postElements = posts.map(post => (
        <Post
            name={post.name}
            postImage={post.postImage}
            stars={post.stars}
            country={post.country}
            cost={post.cost}
            soldOut={post.soldOut}
            key={post.key}
        />
    ))

    return (
        <div className="container">
            <div id="posts-row" className="row p-0 mb-5">
                {postElements}
            </div>
        </div>
    )
}