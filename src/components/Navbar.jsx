import logo from "/src/assets/resources/images/airbnb-logo.png"

export default function Navbar() {
    return (
        <nav className="navbar bg-white shadow-sm">
            <div className="container-fluid ms-3 me-3">
                <a className="navbar-brand" href="#">
                    <img src={logo} alt="Logo"
                         className="d-inline-block align-text-top"/>
                </a>
            </div>
        </nav>
    )
}