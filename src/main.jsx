import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import './assets/index.css'

const page = (
    <React.StrictMode>
        <App/>
    </React.StrictMode>
)
ReactDOM.createRoot(document.getElementById('root')).render(page)