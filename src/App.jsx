import Navbar from "./components/Navbar"
import Header from "./components/Header"
import Posts from "./components/Posts"

export default function App() {
    return (
    <div>
        <Navbar/>
        <Header/>
        <Posts/>
    </div>
  )
}
